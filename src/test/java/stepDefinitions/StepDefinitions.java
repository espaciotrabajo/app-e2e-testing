package stepDefinitions;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.saucelabs.saucerest.SauceREST;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import util.SauceUtils;

public class StepDefinitions {

	private WebDriver driver;
	private String sessionId;
	private WebDriverWait wait;

	private String username = System.getenv("SAUCE_USERNAME");
	private String accesskey = System.getenv("SAUCE_ACCESS_KEY");

	private final String BASE_URL = "https://www.saucedemo.com";
	private SauceUtils sauceUtils;


	@Given("Ingreso a la pagina de inicio de sesion")
	public void ingreso_a_la_pagina_de_inicio_de_sesion() {
		driver.get(BASE_URL);
	}

	@When("Ingreso el nombre de usuario con {string}")
	public void ingreso_el_nombre_de_usuario_con(String usuario) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("user-name")));
		driver.findElement(By.id("user-name")).sendKeys(usuario);
	}

	@When("Ingreso la clave de usuario con {string}")
	public void ingreso_la_clave_de_usuario_con(String clave) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
		driver.findElement(By.id("password")).sendKeys(clave);
	}

	@When("Le doy clic en Login")
	public void le_doy_clic_en_Login() {
		driver.findElement(By.className("btn_action")).click();
	}

	@Then("Se muestra a lista de productos")
	public void se_muestra_a_lista_de_productos() {
		Assert.assertTrue(driver.findElement(By.id("inventory_container")).isDisplayed());
	}
	
	@Then("No se muestra a lista de productos")
	public void no_se_muestra_a_lista_de_productos() {
		Assert.assertEquals(driver.findElements(By.id("inventory_container")).size(), 0);
	}
	
	@Before
	public void setUp(Scenario scenario) throws MalformedURLException {

//		ChromeOptions caps = new ChromeOptions();
//		caps.setCapability("version", "72.0");
//		caps.setCapability("platform", "Windows 10");	
//		caps.setExperimentalOption("w3c", true);
		
		FirefoxOptions caps = new FirefoxOptions();
		caps.setCapability("version", "40");
		caps.setCapability("platform", "Windows 8.1");

		MutableCapabilities sauceOptions = new MutableCapabilities();
		sauceOptions.setCapability("username", username);
		sauceOptions.setCapability("accessKey", accesskey);
		sauceOptions.setCapability("seleniumVersion", "3.141.59");
		sauceOptions.setCapability("name", scenario.getName());

		caps.setCapability("sauce:options", sauceOptions);

		String SAUCE_REMOTE_URL ="https://yusselLuna:22b4a4cb-2edd-4fa0-8161-0ad78ffe307b@ondemand.us-west-1.saucelabs.com:443/wd/hub";// "https://ondemand.saucelabs.com/wd/hub";
		driver = new RemoteWebDriver(new URL(SAUCE_REMOTE_URL), caps);
		sessionId = ((RemoteWebDriver) driver).getSessionId().toString();
		wait = new WebDriverWait(driver, 10);

		SauceREST sauceREST = new SauceREST(username, accesskey);
		sauceUtils = new SauceUtils(sauceREST);
	}

	@After
	public void tearDown(Scenario scenario) {
		driver.quit();
		sauceUtils.updateResults(!scenario.isFailed(), sessionId);
	}

}
