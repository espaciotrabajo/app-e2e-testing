@login
Feature: Acceso al sistema
  Como usuario
  Quiero iniciar sesion en la aplicacion web

  @ok
  Scenario Outline: Login con credenciales validos
    Given Ingreso a la pagina de inicio de sesion
    When Ingreso el nombre de usuario con "<username>"
    And Ingreso la clave de usuario con "<password>"
    And Le doy clic en Login
    Then Se muestra a lista de productos

    Examples: 
      | username      | password     |
      | standard_user | secret_sauce |

  @error
  Scenario Outline: Login con credenciales invalidos
    Given Ingreso a la pagina de inicio de sesion
    When Ingreso el nombre de usuario con "<username>"
    And Ingreso la clave de usuario con "<password>"
    And Le doy clic en Login
    Then No se muestra a lista de productos

    Examples: 
      | username     | password     |
      | doesnt_exist | secret_sauce |
